<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

function register($login, $password) {
	if(strlen($login) > 32) {
		die("zbyt długi login");
	} 

	$db = mysqli_connect("localhost", "root", "", "trollownia");
	if(!$db) die(mysqli_connect_error());
	mysqli_set_charset($db, "utf8mb4");
	
	$clear_login = mysqli_real_escape_string($db, htmlspecialchars($login));
	
	if($clear_login != $login or str_contains($login, "&") or str_contains($login, "#") or str_contains($login, "?")) {
		die("niedozwolone znaki w nazwie użytkownika");
	}

	$hashed_password = password_hash($password, PASSWORD_DEFAULT);
	
	$sql = "INSERT INTO users VALUES('$clear_login', '$hashed_password', 'images/user.png', 0, '', NOW())";
	$query = mysqli_query($db, $sql);
	if(!$query) die(mysqli_error($db));
	
	mysqli_close($db);
	
	login($clear_login, $password);
}

function login($login, $password) {
	$db = mysqli_connect("localhost", "root", "", "trollownia");
	if(!$db) die(mysqli_connect_error());
	mysqli_set_charset($db, "utf8mb4");
	
	$clear_login = mysqli_real_escape_string($db, htmlspecialchars($login));
	
	
	$sql = "SELECT password, admin FROM users WHERE username = '$clear_login'";
	$query = mysqli_query($db, $sql);
	if(!$query) die(mysqli_error($db));
	if(mysqli_num_rows($query) > 1) die("nieoczekiwany błąd");
	
	$hashed_password = mysqli_fetch_row($query)[0];
	$is_admin = mysqli_fetch_row($query)[1];
	
	mysqli_close($db);
	
	if(password_verify($password, $hashed_password)) {
		$_SESSION['user'] = $login;
		if($is_admin) $_SESSION['admin'] = true;
		header("Location: index.php");
	}
}

function logout() {
	$_SESSION = [];
	header("Location: index.php");
}

session_start();

if(isset($_GET['logout'])) {
	logout();
}

if(isset($_SESSION['user'])) {
	header("Location: index.php");
}

if($_POST) {
	$login = $_POST['login'];
	$password = $_POST['password'];
	if(isset($_POST['register'])) {
		register($login, $password);
	} else {
		login($login, $password);
	}
}

?>

<!DOCTYPE html>
<html lang="pl">
	<head>
		<meta charset="utf-8">
    <meta property="og:title" content="trollownia.pl">
    <meta property="og:description" content="trollownia.pl">
    <meta property="og:image" content="images/troll.png">
    <title>trollownia.pl</title>
    <link rel="stylesheet" href="styles.css">
    <link rel="icon" href="images/favicon.ico">
    <style>
    	html, body, .centerhv {
				height: 100%;
			}
			#a {
				position:relative;
				left:1px;
			}
    </style>
	</head><body>
		<div class="centerhv">
			<form method="POST">
				<img src="images/banner.png" width="100%" height="64" title="troll"><br>
				login: 
				<input type="text" name="login" required autofocus><br>
				hasło:
				<input type="password" name="password" required><br>
				<label for="register">rejestracja</label>
				<input type="checkbox" name="register" id="register" onclick="changeText()"><br>
				<input type="submit" value="zaloguj" id="loginButton">
				
				<script>
					function changeText() {
						var button = document.getElementById("loginButton");
						button.value = (button.value == "zaloguj") ? "zarejestruj" : "zaloguj";
					}
				</script>
			</form>
		</div>
	</body>
</html>