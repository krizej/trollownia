<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

$post_id = 0;
if(isset(array_keys($_GET)[0])) {
	$post_id = array_keys($_GET)[0];
} else {
	header("Location: index.php");
}

if($_POST) {
	$db = mysqli_connect("localhost", "root", "", "trollownia");
	if(!$db) die(mysqli_connect_error());
	mysqli_set_charset($db, "utf8mb4");
	
	$content = mysqli_real_escape_string($db, htmlspecialchars($_POST['content']));
	$author = mysqli_real_escape_string($db, htmlspecialchars($_SESSION['user']));
	
	$sql = "INSERT INTO comments(post_id, content, author, date) VALUES($post_id, '$content', '$author', NOW())";
	$query = mysqli_query($db, $sql);
	if(!$query) die(mysqli_error($db));
	
	mysqli_close($db);
}

?>

<!DOCTYPE html>
<html lang="pl">
	<head>
		<meta charset="utf-8">
    <meta property="og:title" content="trollownia.pl">
    <meta property="og:description" content="trollownia.pl">
    <meta property="og:image" content="images/troll.png">
    <title>trollownia.pl</title>
    <link rel="stylesheet" href="styles.css">
    <link rel="icon" href="images/favicon.ico">
	</head><body onload="poopsite()">
		<div class="banner centerh">
			<table>	
				<tr>
					<td>
						<a href='index.php'><button id="mpb">strona główna</button></a>
						<script>function poopsite() { if(Math.random() * 15 < 1) { document.getElementById("mpb").innerHTML = "strona gówna"; }}</script>
					</td>
					<td>
						<img src="images/banner.png" title="baner" height="64">
					</td>
					<td>
						<?php
							if(isset($_SESSION['user'])) {
								echo "<a href='user.php'><button>mój profil</button></a>";
								echo "<a href='login.php?logout' class='smalltext'>wyloguj</a>";
							} else {
								echo "<a href='login.php'><button>login/rejestracja</button></a>";
							}
						?>
					</td>
				</tr>
			</table>
		</div>
		<h1>dupsko</h1>
		<br><br>
		<div class="centerh">
			<div class="userposts">
				<?php
					$db = mysqli_connect("localhost", "root", "", "trollownia");
					if(!$db) die(mysqli_connect_error());
					$sql = "SELECT content, author, date, image FROM posts JOIN users ON posts.author = users.username WHERE id = $post_id";
					$query = mysqli_query($db, $sql);
					if(!$query) die(mysqli_error($db));
					
					while($row = mysqli_fetch_row($query)) {
						echo <<<HTML
							<fieldset class="postcomment">
								<img src="{$row[3]}" class="avatar" alt="avatar" title="avatar" width="64" height="64">
								<legend><a href="user.php?{$row[1]}">{$row[1]}</a></legend>
								<img src="{$row[0]}" class="postimg" alt="{$row[0]}">
								<br><br><small class="smalltext">{$row[2]}</small>
							</fieldset>
							<h2></h2>
						HTML;
					}
				?>
			</div>
		</div>
		<br><br>
		
		<div class="centerh">
			<?php
				if(isset($_SESSION['user'])) {
					echo <<<HTML
						<form method="POST" id="f">
							komentarz:<br>
							<textarea form="f" name="content" required autofocus maxlength=1000></textarea><br><br>
							<div class="centerh"><input type="submit" value="prześlij"></div>
						</form>
					HTML;
				}
			?>
		</div>
		<div class="centerh">
			<div class="userposts">
				<h2>komentarze</h2>
				<?php
					$db = mysqli_connect("localhost", "root", "", "trollownia");
					if(!$db) die(mysqli_connect_error());
					$sql = "SELECT content, author, date, image FROM comments JOIN users ON comments.author = users.username WHERE post_id = $post_id ORDER BY id DESC";
					$query = mysqli_query($db, $sql);
					if(!$query) die(mysqli_error($db));
					
					while($row = mysqli_fetch_row($query)) {
						echo <<<HTML
							<fieldset class="postcomment">
								<img src="{$row[3]}" class="avatar" alt="avatar" title="avatar" width="64" height="64">
								<legend><a href="user.php?{$row[1]}">{$row[1]}</a></legend>
								{$row[0]}
								<br><br><small class="smalltext">{$row[2]}</small>
							</fieldset>
							<h2></h2>
						HTML;
					}
				?>
			</div>
		</div>
	</body>
</html>