<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

$db = mysqli_connect("localhost", "root", "", "trollownia");
if(!$db) die(mysqli_connect_error());
mysqli_set_charset($db, "utf8mb4");

if(isset(array_keys($_GET)[0])) {
	var_dump($_SERVER['QUERY_STRING']);
	$user = $_SERVER['QUERY_STRING'];
} else {
	if(isset($_SESSION['user'])) {
		$user = $_SESSION['user'];
	} else {
		header("Location: index.php");
	}
}

$user = mysqli_real_escape_string($db, htmlspecialchars($user));


$sql = "SELECT username, image, description, join_date, admin FROM users WHERE username = '$user'";
$query = mysqli_query($db, $sql);

if(!$query) die(mysqli_error($db));

$user_data = mysqli_fetch_row($query);

?>

<!DOCTYPE html>
<html lang="pl">
	<head>
		<meta charset="utf-8">
    <meta property="og:title" content="trollownia.pl">
    <meta property="og:description" content="trollownia.pl">
    <meta property="og:image" content="images/troll.png">
    <title>trollownia.pl</title>
    <link rel="stylesheet" href="styles.css">
    <link rel="icon" href="images/favicon.ico">
	</head><body onload="poopsite()">
		<div class="banner centerh">
			<table>	
				<tr>
					<td>
						<a href='index.php'><button id="mpb">strona główna</button></a>
						<script>function poopsite() { if(Math.random() * 15 < 1) { document.getElementById("mpb").innerHTML = "strona gówna"; }}</script>
					</td>
					<td>
						<img src="images/banner.png" title="baner" height="64">
					</td>
					<td>
						<?php
							if(isset($_SESSION['user'])) {
								echo "<a href='user.php'><button>mój profil</button></a>";
								echo "<a href='login.php?logout' class='smalltext'>wyloguj</a>";
							} else {
								echo "<a href='login.php'><button>login/rejestracja</button></a>";
							}
						?>
					</td>
				</tr>
			</table>
		</div>
		
		<div class="userpage centerh">
			<div class="avatarcontainer">
				<?php
					echo "<img src='{$user_data[1]}' alt='awatar' title='awatar' width=128 height=128>";
				?>
			</div>
			<div class="infocontainer">
				<?php
					echo "<h2>{$user_data[0]}";
					if($user_data[4]) {
						echo "&nbsp<img src='images/badges/admin.png' class='badge' title='admin' width=16>";
					}
					echo "</h2>";
					
					echo "<p>{$user_data[2]}</p>";
				
					echo "<small class='smalltext'>dołączył: {$user_data[3]}</small>";
					if(isset($_SESSION['user'])) {
						if($_SESSION['user'] == $user) {
							echo "<a href='editprofile.php'><button class='editbutton'>edytuj profil</button></a>";
						}
					}
					
				?>
			</div>
		</div>
		
		<div class="centerh">
			<div class="userposts">
				<a href="#wpisy">przejdź do wpisów</a>
				<a href="#komentarze">przejdź do komentarzy</a>
				<h1 id="wpisy">wpisy</h1>
				<?php
					$sql = "SELECT content, author, date, id, image FROM posts JOIN users ON posts.author = users.username WHERE author = '$user' ORDER BY id DESC";
					$query = mysqli_query($db, $sql);
					if(!$query) die(mysqli_error($db));
					
					if(mysqli_num_rows($query) == 0) {
						echo "<h3 class='smalltext'>&nbsp&nbsp&nbsp&nbspbrak</h3>";
					}
					
					while($row = mysqli_fetch_row($query)) {
							echo <<<HTML
								<fieldset class="postcomment">
									<img src="{$row[4]}" class="avatar" alt="avatar" title="avatar" width="64" height="64">
									<legend>{$row[1]}</legend>
									<img src="{$row[0]}" alt="{$row[0]}">
									<br><br><small class="smalltext">{$row[2]}</small>
									<a href="post.php?{$row[3]}"><button>komentarze</button></a>
								</fieldset>
								<h2></h2>
							HTML;
					}
				?>
				<h1 id="komentarze">komentarze</h1>
				<?php
					$sql = "SELECT content, author, date, post_id, image FROM comments JOIN users ON comments.author = users.username WHERE author = '$user' ORDER BY id DESC";
					$query = mysqli_query($db, $sql);
					if(!$query) die(mysqli_error($db));
					
					if(mysqli_num_rows($query) == 0) {
						echo "<h3 class='smalltext'>&nbsp&nbsp&nbsp&nbspbrak</h3>";
					}
					
					while($row = mysqli_fetch_row($query)) {
							echo <<<HTML
								<fieldset class="postcomment">
									<img src="{$row[4]}" class="avatar" alt="avatar" title="avatar" width="64" height="64">
									<legend>{$row[1]}</legend>
									<img src="{$row[0]}" alt="{$row[0]}">
									<br><br><small class="smalltext">{$row[2]}</small>
									<a href="post.php?{$row[3]}"><button>wpis</button></a>
								</fieldset>
								<h2></h2>
							HTML;
					}
				?>
			</div>
		</div>
	</body>
</html>