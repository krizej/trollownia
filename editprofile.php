<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

session_start();

$db = mysqli_connect("localhost", "root", "", "trollownia");
if(!$db) die(mysqli_connect_error());
mysqli_set_charset($db, "utf8mb4");

if(isset($_SESSION['user'])) {
	$user = $_SESSION['user'];
} else {
	header("Location: index.php");
}

$user = mysqli_real_escape_string($db, htmlspecialchars($user));

$sql = "SELECT image, description FROM users WHERE username = '$user'";

$query = mysqli_query($db, $sql);

if(!$query) die(mysqli_error($db));

$user_data = mysqli_fetch_row($query);

if($_POST) {
	$avatar = $_POST['avatar'] ?? $user_data[0];
	$description = $_POST['description'] ?? $user_data[1];
	
	if(strlen($description) > 100) {
		$description = substr($description, 0, 100);
	}
	
	$avatar = mysqli_real_escape_string($db, htmlspecialchars($avatar));
	$description = mysqli_real_escape_string($db, htmlspecialchars($description));
	
	$sql = "UPDATE users SET image = '$avatar', description = '$description' WHERE username = '$user'";
	$query = mysqli_query($db, $sql);
	if(!$query) die(mysqli_error($db));
	header("Location: user.php");
}


?>

<!DOCTYPE html>
<html lang="pl">
	<head>
		<meta charset="utf-8">
    <meta property="og:title" content="trollownia.pl">
    <meta property="og:description" content="trollownia.pl">
    <meta property="og:image" content="images/troll.png">
    <title>trollownia.pl</title>
    <link rel="stylesheet" href="styles.css">
    <link rel="icon" href="images/favicon.ico">
    <style>
    	#d {
    		position: relative;
    		right: 2px;
    	}
    </style>
	</head><body onload="poopsite()">
		<div class="banner centerh">
			<table>	
				<tr>
					<td>
						<a href='index.php'><button id="mpb">strona główna</button></a>
						<script>function poopsite() { if(Math.random() * 15 < 1) { document.getElementById("mpb").innerHTML = "strona gówna"; }}</script>
					</td>
					<td>
						<img src="images/banner.png" title="baner" height="64">
					</td>
					<td>
						<?php
							if(isset($_SESSION['user'])) {
								echo "<a href='user.php'><button>mój profil</button></a>";
								echo "<a href='login.php?logout' class='smalltext'>wyloguj</a>";
							} else {
								echo "<a href='login.php'><button>login/rejestracja</button></a>";
							}
						?>
					</td>
				</tr>
			</table>
		</div>
		<h1>dupsko</h1>
		<div class="userpage centerh">
			<form method="POST">
				awatar: <input type="text" name="avatar" value='<?php echo $user_data[0]; ?>'>
				<img src="images/help.png" alt=" ? " title="link do pliku
domyślny awatar: images/user.png" class="help"><br><br>
				opis: &nbsp&nbsp&nbsp&nbsp<input type="text" name="description" id="d" value='<?php echo $user_data[1]; ?>'>
				<img src="images/help.png" alt=" ? " title="maksymalna długość: 100 znaków" class="help"><br><br>
				<div class="centerh"><input type="submit" value="zapisz"></div>
			</form>
		</div>
	</body>
</html>